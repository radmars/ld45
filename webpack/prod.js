// production config
const merge = require('webpack-merge');
const {resolve} = require('path');
const CopyPlugin = require('copy-webpack-plugin');

const commonConfig = require('./common');

module.exports = merge(commonConfig, {
	mode: 'production',
	entry: './index.tsx',
	output: {
		filename: 'js/bundle.[hash].min.js',
		path: resolve(__dirname, '../dist'),
		publicPath: './',
	},
	devtool: 'source-map',
	plugins: [
		new CopyPlugin([
			{ from: "./**/*.m4a", to: "src" },
			{ from: "./**/*.ogg", to: "src" }
		]),
	],
});
