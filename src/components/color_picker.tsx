import { hot } from 'react-hot-loader';
import * as React from 'react';
import reactCSS from 'reactcss'
import { SketchPicker  } from 'react-color'

export interface Color {
	r: string;
	g: string;
	b: string;
	a: string;
}

interface ColorPickerState {
	displayColorPicker: boolean;
	color: Color;
}

interface ColorPickerProps {
	onColorChange: (c: Color) => void;
	color: Color;
}

class ColorPickerComp extends React.Component<ColorPickerProps, ColorPickerState> {
	state = {
		displayColorPicker: false,
		color: {
			r: '241',
			g: '112',
			b: '19',
			a: '1',
		},
	};

	constructor(props: ColorPickerProps, context) {
		super(props, context)
		this.state.color = props.color;
	}

	handleClick = () => {
		this.setState({ displayColorPicker: !this.state.displayColorPicker })
	};

	handleClose = () => {
		this.setState({ displayColorPicker: false })
	};

	handleChange = (color) => {
		this.props.onColorChange(color.rgb);
		this.setState({ color: color.rgb })
	};

	render() {

		const styles = reactCSS({
			'default': {
				color: {
					width: '36px',
					height: '14px',
					borderRadius: '2px',
					background: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,
				},
				swatch: {
					background: '#fff',
					borderRadius: '1px',
				},
				popover: {
					position: 'absolute',
					zIndex: '2',
				},
				cover: {
					position: 'fixed',
					top: '0px',
					right: '0px',
					bottom: '0px',
					left: '0px',
				},
			},
		});

		return (
			<span>
			<button onClick={ this.handleClick }>
				<span style={ styles.color }>&nbsp;&nbsp;&nbsp;&nbsp;</span>
			</button>

			{ this.state.displayColorPicker ? <div style={ styles.popover }>
				<div style={ styles.cover } onClick={ this.handleClose }/>
				<SketchPicker color={ this.state.color } onChange={ this.handleChange } />
				</div> : null }
			</span>
		)
	}
}

declare let module: object;
export const ColorPicker = hot(module)(ColorPickerComp);
