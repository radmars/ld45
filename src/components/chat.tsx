import { hot } from 'react-hot-loader';
import * as React from 'react';
import "./chat.scss";
import { Screen, Clippy, ClippySingleton } from './clippy';

interface Message {
	msg: string;
}

interface ChatState {
	messages: Message[];
}

class ChatComp extends React.Component<{}, ChatState> {
	public state = {
		messages: [],
	}

	public async componentDidMount() {
		ClippySingleton.playSound("compilation");
		await ClippySingleton.delay(2000);
		ClippySingleton.speak("New chat message from BigBoss420");
		this.addMessage("hey dude are u ok?");
		await ClippySingleton.delay(2000);
		this.addMessage("ur folks asked me to check in on you im almost there");
		await ClippySingleton.delay(4000);
		this.addMessage("i know ur busy doing the game jam thing this weekend but i havent heard anything from you in forever. getting worried");
		await ClippySingleton.delay(4000);
		this.addMessage("im outside");
		await ClippySingleton.delay(3000);
		this.addMessage("can you let me in im outside");
		await ClippySingleton.delay(6000);
		this.addMessage("dude what is that weird noise");
		await ClippySingleton.delay(5000);
		this.addMessage("wtf is that WHAT IS THAT OH FU");
		await ClippySingleton.delay(1500);
		ClippySingleton.stopAmbience2();
		ClippySingleton.changeScreen(Screen.GameOver);
	}

	private addMessage(msg: string) {
		ClippySingleton.playSound("aim");
		const messages = this.state.messages;
		messages.push(msg);
		this.setState({messages});
	}

	public render() {
		return (
			<div id='chat-window'>
				<div className='border-box'>
					<div className='title-bar'>
						💬 Chat
					</div>
					<div className='message-window'>
						{this.state.messages.map((message, i) => {
							return <div className='message' key={i}>
								<span className='user'>@BigBoss420</span>: <span className='message'>{message}</span>
							</div>
						})}
					</div>
					<input disabled={true}></input>
				</div>
			</div>
		);
	}
}

declare let module: object;
export default hot(module)(ChatComp);
