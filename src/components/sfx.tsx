import { hot } from 'react-hot-loader';
import * as React from 'react';
import "./sfx.scss";
import {Window} from './window';
import { ClippySingleton, Screen } from './clippy';
import {ReactMic} from 'react-mic';

interface EditorState {
	recording: boolean;
	replaying: boolean;
	current: Recording;
}

enum Recording {
	Action,
	Voice,
	Them,
}

// This is awkward the component isn't actually active while this is going
// on...
async function toSigil() {
	ClippySingleton.changeScreen(Screen.Sigil);
	ClippySingleton.prompt(
		"Great! All it takes is one more thing to bring your beautiful creation into the world! Just draw the sigil.",
		<span>Start <em>drawing</em> now.</span>);
}

class SfxEditor extends React.Component<{}, EditorState> {
	public state = {
		recording: false,
		current: Recording.Action,
		replaying: false,
	}

	private reset() {

	}

	private async replayAndUpdate(next: Recording, sound: string) {
		this.setState({ recording: false, current: next, replaying: true});
		await ClippySingleton.playSound(sound);
		this.setState({replaying: false});
	}

	private async onStop() {
		switch (this.state.current) {
			case Recording.Action:
				await this.replayAndUpdate(Recording.Voice, "cronch");
				await ClippySingleton.speak("Very evocative! Now record some voice clips for the main character. Just be yourself!");
				break;
			case Recording.Voice:
				await this.replayAndUpdate(Recording.Them, "scream");
				await ClippySingleton.speak("Sounds great! What do your new friends sound like?");
				break;
			case Recording.Them:
				await this.replayAndUpdate(Recording.Them, "friends");
				await ClippySingleton.prompt("Now, why don’t you finish writing the story?",
					<p>
					Go back to the <a href='#' onClick={async () => {
						ClippySingleton.addText("They’re coming. I can hear them outside. I was just trying to make a game I didn’t know what I was doing I know I went too far this time I just need to stop now don’t think about it just turn it off JUST TURN IT OFF PLEASE STOP PLEASE THIS CANT BE HAPPENING HELP ME PLEASE GOD STOP TURN IT OFF PLEASE");
						ClippySingleton.changeScreen(Screen.Design);
						await ClippySingleton.delay(1000);
						ClippySingleton.prompt(
							"When you're done, press submit.",
							<button onClick={() => toSigil()}> submit </button>
						);
					}}>story</a> editor.
					</p>);
				break;
		}
	}

	public render() {
		return (
			<Window
				title="🖌️Shoutmachine 0.1 Alpha"
				onReset={() => this.reset()} >
				<div className='sfx-widget'>
					<ReactMic
						record={this.state.recording}
						onStop={() => this.onStop()}
						/>
					<br />
					<button onClick={() => this.setState({ recording: !this.state.recording})  }
						disabled={this.state.replaying}
						> 🎙️
						{ (() => this.state.recording ? "Stop" : (this.state.replaying ? "Playing..." : "Record"))() }
					</button>
				</div>
			</Window>
		)
	}
}

declare let module: object;
export const SFX = hot(module)(SfxEditor);
