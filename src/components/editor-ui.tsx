import { hot } from 'react-hot-loader';
import { Window } from './window';
import * as React from 'react';
import "./editor-ui.scss";
import { ClippySingleton, Screen } from './clippy';

interface EditorState {
	mounted: boolean;
	name: string;
	prompted: boolean;
	text: string;
	canDisable: boolean;
}

class Editor extends React.Component<{}, EditorState> {
	public state = {
		canDisable: true,
		mounted: false,
		name: "",
		prompted: false,
		text: "",
	}

	private textarea: HTMLTextAreaElement | null;

	public componentDidMount() {
		this.textarea.focus();
		const clippyText = ClippySingleton.state.text
		this.setState({ mounted: true, text: clippyText, prompted: clippyText != "" });
	}

	private async delayAppend(new_text: string): Promise<void> {
		let text = this.state.text;
		return new Promise(async resolve => {
			for(const c of new_text) {
				text += c;
				this.setState({text});
				this.textarea.blur();
				this.textarea.focus();
				var rand = Math.floor(Math.random() * 5) + 1;
				ClippySingleton.playSound("type" + rand.toString());
				await ClippySingleton.delay(30);
			}
			ClippySingleton.setText(text);
			resolve();
		})
	}

	public async componentDidUpdate() {
		ClippySingleton.startAmbience1();
		// Kick off the whole chain of events by waiting for the first update.
		if(!this.state.prompted) {
			this.showMessage1();
		}
	}

	public async showMessage1() {
		this.setState({ prompted: true });
		await ClippySingleton.delay(10000);
		ClippySingleton.playSound("intro");
		await ClippySingleton.delay(500);
		const message = "Hi! I’m Writey! Looks like you’re having some trouble coming up with ideas for your game. I can help!";
		ClippySingleton.prompt(message,
			<div>
				<button onClick={() => this.showMessage2()}>Ok</button>
				<button disabled={!this.state.canDisable} onClick={() => this.showCantDoThat()}>Disable writey</button>
			</div>
		);
	}

	private async showCantDoThat() {
		this.setState({canDisable: false});
		await ClippySingleton.speak("I'm afraid I can't do that...");
		await ClippySingleton.delay(4000);
		this.showMessage1();
	}

	public showMessage2() {
		const message = "First, what’s your name?";
		ClippySingleton.prompt(message,
			<div>
				<label>
					Your name:
					<input onChange={(e) => this.setState({ name: e.target.value})} />
				</label>
				<button onClick={() => this.showMessage3()}>Ok</button>
			</div>
		);
	}

	public async showMessage3() {
		await ClippySingleton.speak("Let's see what you have so far...");

		await ClippySingleton.delay(1000);

		await ClippySingleton.speak("This is awful. Let's start over.");

		// TODO: Some sort of animation for trashing the content?
		this.setState({text: ""});

		const gameType = await ClippySingleton.promptInput(
			"What kind of game is it?",
			[
				['action', 'Action'],
				['rpg', 'RPG'],
				['horror', 'Horror'],
			]
		);

		switch (gameType) {
			case 'action': await ClippySingleton.speak("Great choice!"); await this.delayAppend("ACTION: Relentless violence. Defeat the enemies, if you can.\n"); break;
			case 'horror': await ClippySingleton.speak("My favorite."); await this.delayAppend("HORROR: Run. Hide. They will not stop until they find you.\n"); break;
			case 'rpg':    await ClippySingleton.speak("OK!"); await this.delayAppend("RPG: Gain XP and knowledge. What happens when you go too far?\n"); break;
		}

		const characterType = await ClippySingleton.promptInput(
			"What's the main character like?",
			[
				[ 'cool', 'Cool'],
				[ 'strong', 'Strong'],
				[ 'smart', 'Smart'],
			],
		);

		await this.delayAppend("\n");

		switch(characterType) {
			case 'cool':   await this.delayAppend(`${this.state.name} thinks they're cool, but does that matter in the end?\n`); break;
			case 'strong': await this.delayAppend(`${this.state.name} is strong, but will that be enough to save them?\n`); break;
			case 'smart':  await this.delayAppend(`${this.state.name} is smart, but that doesn't mean they can figure out how to survive this.\n`); break;
		}

		await this.delayAppend("\n");
		await this.delayAppend("All cooped up in their room, working at their computer.\n");
		await this.delayAppend("They have no idea what their curiosity has gotten them into.\n");

		await ClippySingleton.delay(500);

		await ClippySingleton.promptInput(
			"What's the setting?",
			[
				['apartment', 'An apartment'],
				['room', 'Your room'],
				['home', 'Home'],
			]
		);

		await this.delayAppend("\nSETTING: A nondescript room.\n");
		await this.delayAppend("The walls are bare, no decoration or personality in sight.\n");
		await this.delayAppend(`${this.state.name} is there.\n\n`);
		await this.delayAppend("Alone.\n\n");
		await this.delayAppend("Oblivious to their surroundings, they are completely absorbed.\n");
		await this.delayAppend("Completely helpless.\n");

		const mechanics = await ClippySingleton.promptInput(
			"How about some game mechanics?",
			[
				['action', 'Action'],
				['rpg', 'RPG',],
				['horror', 'Horror'],
			],
		);

		await this.delayAppend("\nMECHANICS: ");

		switch(mechanics) {
			case 'action': await this.delayAppend("There are too many of them. You can't stop them all.\n"); break;
			case 'horror': await this.delayAppend("Run. You have to run while you still can.\n"); break;
			case 'rpg':    await this.delayAppend("You can grind for XP all you want, but it won't help you in the end.\nWhat is the price of knowledge?\n"); break;
		}

		await ClippySingleton.speak("Let's make the achievement list!");

		await this.delayAppend("\nACHIEVEMENTS:\n");
		await this.delayAppend("☐ Obtain your first weapon (0% of players have earned this)\n");
		await this.delayAppend("☐ Kill your first enemy (0% of players have earned this)\n");
		await this.delayAppend("☑ Start the document (15% of players have earned this)\n");
		await this.delayAppend("☐ Obtain the knowledge (14% of players have earned this)\n");
		await this.delayAppend("☐ Successfully call for help (0% of players have earned this)\n");
		await this.delayAppend("☐ Complete the sigil (13% of players have earned this)\n");
		await this.delayAppend("☐ Ascend (13% of players have earned this)\n");

		const enemies = await ClippySingleton.promptInput(
			"What are the enemies like?",
			[
				['not scary', 'Not scary at all'],
				['scary', 'Scary'],
				['very', 'Very scary'],
			],
		);

		await this.delayAppend("\nENEMIES: ");

		await this.delayAppend("The more you think about them, the more they come into focus.\nBlurry at first, then the details appear.\n");
		await this.delayAppend("Initial reports were detailed to a fault - all those who investigated were never seen again.\n");
		await this.delayAppend("Even this description may be too much.\n");

		switch(enemies) {
			case 'scary': await this.delayAppend("After each attack, their reported number increased, as well as the frequency of incidents.\nThe attacks became even more gruesome. \nFriends and colleagues of the victims were drawn to them, compelled to learn more, beckoned by strange dreams and premonitions.\n"); break;
			case 'very':  await this.delayAppend("After each attack, their reported number increased, as well as the frequency of incidents.\nThe attacks became even more gruesome. \nThe blood... I can't ever forget the blood.\nGame developers were unusually predisposed to become victims.\nFriends and colleagues of the victims were drawn to them, compelled to learn more, beckoned by strange dreams and premonitions.\nOnce you see the sign there is no going back.\n"); break;
		}

		await this.delayAppend("\nIt's too late for you now.\n\n");

		await ClippySingleton.promptInput(
			`Then what did ${this.state.name} do?`,
			[['hahahahahahahahahaha', 'Call for help']]
		);

		await this.delayAppend(`\n${this.state.name} called for help\n\n    but nobody came.\n\n\n`);

		await ClippySingleton.delay(1000);

		await ClippySingleton.prompt(
			"Okay! Let’s start on the graphics! Draw an environment.",
			<p>
				Switch to the <a href='#' onClick={() => ClippySingleton.changeScreen(Screen.Gfx)}>graphics</a> screen.
			</p>
		);
	}

	private reset() {
		this.setState({ prompted: false });
	}

	public render() {
		return (
			<Window
				title="🗎 Game Designer Pro 98: Story Editor"
				onReset={() => this.reset()} >
				<textarea
					value={this.state.text}
					ref={(input) => this.textarea = input}
					onChange={(e) => this.setState({ text: e.target.value}) }>
				</textarea>
			</Window>
		)
	}
}

declare let module: object;
export default hot(module)(Editor);
