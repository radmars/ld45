import { hot } from 'react-hot-loader';
import * as React from 'react';
import "./app.scss";
import Editor from './editor-ui';
import {GFX} from './gfx';
import {Sigil} from './sigil';
import {SFX} from './sfx';
import {GameOver} from './gameover';
import { Screen, Clippy } from './clippy';

const loading = require('../assets/tshell32_168.gif');

interface AppState {
	loaded: boolean;
	percent: number;
	window: Screen;
}

class App extends React.Component<{}, AppState> {
	public state = {
		loaded: false,
		percent: 0,
		window: Screen.Design,
	};

	private updateLoader(interval: number) {
		const percent = this.state.percent + 2;
		const loaded = percent >= 100;
		if(loaded) {
			window.clearInterval(interval);
		}
		this.setState({ percent, loaded });
	}

	public componentDidMount() {
		const interval = window.setInterval( () => this.updateLoader(interval), 100 );
	}

	public render() {
		if(!this.state.loaded) {
			return (
				<div id='loading-panel'>
					<div className='centered'>
						<div>
							Creating new game design project...
							<br />
							<img src={loading} />
							<br />
							{this.state.percent}%
						</div>
					</div>
				</div>
			);
		}

		return (
			<>
				<Clippy screenChanger={(s: Screen) => this.setState({window: s})}/>
				{(() => {
					switch(this.state.window) {
						case Screen.Design: return <Editor />;
						case Screen.Sigil:  return <Sigil />;
						case Screen.Gfx:    return <GFX />;
						case Screen.Sfx:    return <SFX />;
						case Screen.GameOver: return <GameOver />;
					}
				})()}
			</>
		);
	}
}

declare let module: object;
export default hot(module)(App);
