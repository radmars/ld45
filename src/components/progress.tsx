import { hot } from 'react-hot-loader';
import * as React from 'react';
import "./progress.scss";

interface ChatState {
	progress: number;
}

class ProgressComp extends React.Component<{}, ChatState> {
	public state = {
		progress: 0,
	}

	public componentDidMount() {
		const start = Date.now();
		const interval = setInterval(() => {
			let progress = (Date.now() - start)/29500;
			if(progress > 1) {
				progress = 1;
				clearInterval(interval);
			}
			this.setState({progress});
		}, 100);
	}

	public render() {
		return (
			<div id='progress-bar'>
				Compiling... {( this.state.progress * 100 ).toFixed(0)} %
				<div className='progress' style={{width: `${this.state.progress * 100}%`}}>
					&nbsp;
				</div>
			</div>
		);
	}
}

declare let module: object;
export default hot(module)(ProgressComp);
