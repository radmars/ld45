import { hot } from 'react-hot-loader';
import * as React from 'react';
import "./gfx.scss";
import {Window} from './window';
import { ClippySingleton } from './clippy';
import reactCSS from 'reactcss'
import { SketchPicker  } from 'react-color'

interface Color {
	r: string;
	g: string;
	b: string;
	a: string;
}

interface ColorPickerState {
	displayColorPicker: boolean;
	color: Color;
}

interface ColorPickerProps {
	onColorChange: (c: Color) => void;
	color: Color;
}

class ColorPicker extends React.Component<ColorPickerProps, ColorPickerState> {
	state = {
		displayColorPicker: false,
		color: {
			r: '241',
			g: '112',
			b: '19',
			a: '1',
		},
	};

	constructor(props: ColorPickerProps, context) {
		super(props, context)
		this.state.color = props.color;
	}

	handleClick = () => {
		this.setState({ displayColorPicker: !this.state.displayColorPicker })
	};

	handleClose = () => {
		this.setState({ displayColorPicker: false })
	};

	handleChange = (color) => {
		this.props.onColorChange(color.rgb);
		this.setState({ color: color.rgb })
	};

	render() {

		const styles = reactCSS({
			'default': {
				color: {
					width: '36px',
					height: '14px',
					borderRadius: '2px',
					background: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,
				},
				swatch: {
					background: '#fff',
					borderRadius: '1px',
				},
				popover: {
					position: 'absolute',
					zIndex: '2',
				},
				cover: {
					position: 'fixed',
					top: '0px',
					right: '0px',
					bottom: '0px',
					left: '0px',
				},
			},
		});

		return (
			<span>
			<button onClick={ this.handleClick }>
				<span style={ styles.color }>&nbsp;&nbsp;&nbsp;&nbsp;</span>
			</button>

			{ this.state.displayColorPicker ? <div style={ styles.popover }>
				<div style={ styles.cover } onClick={ this.handleClose }/>
				<SketchPicker color={ this.state.color } onChange={ this.handleChange } />
				</div> : null }
			</span>
		)
	}
}

enum Picture {
	Environment,
	Character,
	Friends,
	Sigil,
}

type CanvasMouseEvent = React.MouseEvent<HTMLCanvasElement, MouseEvent>;

interface EditorState {
	brushSize: number;
	current: Picture;
}

class GfxEditor extends React.Component<{}, EditorState> {
	public state = {
		brushSize: 5,
		current: Picture.Environment,
	}

	private currentColor: Color = {
		r: '255',
		g: '0',
		b: '0',
		a: '255'
	};
	private previousPoint: [number, number];
	private canvas: HTMLCanvasElement;
	private gfx_context: CanvasRenderingContext2D;

	private reset() {
		this.setState({ current: Picture.Environment });
	}

	private onMouseDown(_event: CanvasMouseEvent) {
		this.previousPoint = null;
	}

	private onMouseMove(event: CanvasMouseEvent) {
		if((event.buttons & 1) == 0) {
			return;
		}

		const point: [number, number] = [
			event.clientX - this.canvas.offsetLeft,
			event.clientY - this.canvas.offsetTop,
		];

		const previousPoint = this.previousPoint;
		this.previousPoint = point;
		if(!previousPoint) {
			return;
		}

		const context = this.gfx_context;
		context.strokeStyle = `rgba(${this.currentColor.r}, ${this.currentColor.g}, ${this.currentColor.b}, ${this.currentColor.a})`;
		context.lineJoin = "round";
		context.lineWidth = this.state.brushSize;
		context.beginPath();
		context.moveTo(previousPoint[0], previousPoint[1]);
		context.lineTo(point[0], point[1]);
		context.closePath();
		context.stroke()
		event.preventDefault();
	}

	private setCanvas(canvas: HTMLCanvasElement) {
		if(canvas) {
			this.canvas = canvas;
			this.gfx_context = canvas.getContext("2d");
		}
	}

	private changeColor(c: Color) {
		this.currentColor = c;
	}

	private async submitPicture() {
		this.gfx_context.clearRect(0, 0, this.canvas.width, this.canvas.height);

		switch (this.state.current) {
			case Picture.Environment:
				this.setState({ current: Picture.Character });
				await ClippySingleton.speak("Wow! It’s as if I can hear them outside right now! Now draw the main character.");
				break;

			case Picture.Character:
				this.setState({ current: Picture.Friends });
				await ClippySingleton.speak("Looks just like you! Now draw your new friends.");
				break;

			case Picture.Friends:
				// TODO: Screen flip logic.
				this.setState({ current: Picture.Sigil });
				await ClippySingleton.speak("Great! Now let’s record some audio. First some action sounds. Really do some damage.");
				break;
		}
	}

	public render() {
		let brushSize = this.state.brushSize;
		return (
			<Window
				title="🖌️Paint Box Jr."
				onReset={() => this.reset()} >
				<div className='toolbar'>
					Brush size: <input value={brushSize} onChange={(e) => this.setState({brushSize: Number.parseInt(e.target.value) || 0})} 
						pattern="[0-9][0-9]*" />
					Brush color:
					<ColorPicker color={this.currentColor}
						onColorChange={(c) => this.changeColor(c)} />
					<button onClick={() => this.submitPicture()}><strong>Submit</strong></button>
				</div>
				<canvas id="gfx-canvas"
					width="640"
					height="480"
					ref={(canvas) => this.setCanvas(canvas)}
					onMouseMove={(e) => this.onMouseMove(e)}
					onMouseDown={(e) => this.onMouseDown(e)}
					>
				</canvas>
			</Window>
		)
	}
}

declare let module: object;
export const GFX = hot(module)(GfxEditor);
