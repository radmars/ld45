import { hot } from 'react-hot-loader';
import * as React from 'react';
import "./gfx.scss";
import {Window} from './window';
import { ClippySingleton, Screen } from './clippy';
import {Color, ColorPicker} from './color_picker';

const face = require('./pics/face.gif');
const room = require('./pics/room.gif');
const friends = require('./pics/friends.gif');
const friends2 = require('./pics/friends2.gif');

enum Picture {
	Environment,
	Character,
	Friends,
	Sigil,
}

const pictures = {
	[Picture.Environment]: [room],
	[Picture.Character]: [face],
	[Picture.Friends]: [friends, friends2],
};


function randomPicture(p: Picture) {
	const items = pictures[p];
	return items[Math.floor(Math.random()*items.length)];
}

type CanvasMouseEvent = React.MouseEvent<HTMLCanvasElement, MouseEvent>;

interface EditorState {
	brushSize: number;
	current: Picture;
}

class GfxEditor extends React.Component<{}, EditorState> {
	public state = {
		brushSize: 5,
		current: Picture.Environment,
	}

	private currentColor: Color = {
		r: '255',
		g: '0',
		b: '0',
		a: '255'
	};
	private previousPoint: [number, number];
	private canvas: HTMLCanvasElement;
	private gfx_context: CanvasRenderingContext2D;

	private reset() {
		this.setState({ current: Picture.Environment });
	}

	private onMouseDown(_event: CanvasMouseEvent) {
		this.previousPoint = null;
	}

	private onMouseMove(event: CanvasMouseEvent) {
		if((event.buttons & 1) == 0) {
			return;
		}

		const point: [number, number] = [
			event.clientX - this.canvas.offsetLeft,
			event.clientY - this.canvas.offsetTop,
		];

		const previousPoint = this.previousPoint;
		this.previousPoint = point;
		if(!previousPoint) {
			return;
		}

		const context = this.gfx_context;
		context.strokeStyle = `rgba(${this.currentColor.r}, ${this.currentColor.g}, ${this.currentColor.b}, ${this.currentColor.a})`;
		context.lineJoin = "round";
		context.lineWidth = this.state.brushSize;
		context.beginPath();
		context.moveTo(previousPoint[0], previousPoint[1]);
		context.lineTo(point[0], point[1]);
		context.closePath();
		context.stroke()
		event.preventDefault();
	}

	private setCanvas(canvas: HTMLCanvasElement) {
		if(canvas) {
			this.canvas = canvas;
			this.gfx_context = canvas.getContext("2d");
		}
	}

	private changeColor(c: Color) {
		this.currentColor = c;
	}

	private async showPicture(p: Picture): Promise<void> {
		return new Promise(resolve => {
			const img = new Image();
			img.addEventListener('load', async () => {
				this.gfx_context.drawImage(img, 0, 0, this.canvas.width, this.canvas.height);
				await ClippySingleton.delay(1000);
				resolve();
			});
			img.src = randomPicture(p);
		});
	}

	private async submitPicture() {
		this.gfx_context.clearRect(0, 0, this.canvas.width, this.canvas.height);
		const current = this.state.current;
		this.showPicture(current);

		switch (current) {
			case Picture.Environment:
				this.setState({ current: Picture.Character });
				await ClippySingleton.speak("Wow! It’s as if I can hear them outside right now! Now draw the main character.");
				break;

			case Picture.Character:
				this.setState({ current: Picture.Friends });
				await ClippySingleton.speak("Looks just like you! Now draw your new friends.");
				break;

			case Picture.Friends:
				// TODO: Screen flip logic.
				this.setState({ current: Picture.Sigil });
				await ClippySingleton.prompt("Great! Now let’s record some audio. First some action sounds. Really do some damage.",
					<p>
						Switch to the <a href='#' onClick={() => ClippySingleton.changeScreen(Screen.Sfx)}>recording</a> screen
					</p>);
				break;
		}
		this.gfx_context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}

	public render() {
		let brushSize = this.state.brushSize;
		return (
			<Window
				title="🖌️Paint Box Jr."
				onReset={() => this.reset()} >
				<div className='toolbar'>
					Brush size: <input value={brushSize} onChange={(e) => this.setState({brushSize: Number.parseInt(e.target.value) || 0})} 
						pattern="[0-9][0-9]*" />
					Brush color:
					<ColorPicker color={this.currentColor}
						onColorChange={(c) => this.changeColor(c)} />
					<button onClick={() => this.submitPicture()}><strong>Submit</strong></button>
				</div>
				<canvas id="gfx-canvas"
					width="640"
					height="480"
					ref={(canvas) => this.setCanvas(canvas)}
					onMouseMove={(e) => this.onMouseMove(e)}
					onMouseDown={(e) => this.onMouseDown(e)}
					>
				</canvas>
			</Window>
		)
	}

	public componentDidMount() {
		ClippySingleton.startAmbience2();
	}
}

declare let module: object;
export const GFX = hot(module)(GfxEditor);
