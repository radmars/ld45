import { hot } from 'react-hot-loader';
import * as React from 'react';
import './bubble.scss';

interface BubbleProps {
	children: React.ReactNode;
}

class Bubble extends React.Component<BubbleProps, {}> {
	public render() {
		return (
			<div className='clippy-bubble'>
				<div className="clippy-bubble-arrow"></div>
				{this.props.children}
			</div>
		);
	}
}

declare let module: object;
export default hot(module)(Bubble);
