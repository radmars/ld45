import { hot } from 'react-hot-loader';
import * as React from 'react';
import './gameover.scss';
import {ClippySingleton} from './clippy';

class GameOverComp extends React.Component<{}, {}> {
	public state = {
	}

	public componentDidMount() {
		ClippySingleton.removeBubble();
	}

	public render() {
		return (
			<div id='gameover'>
				<div>
				 
				</div>
			</div>
		);
	}
}

declare let module: object;
export const GameOver = hot(module)(GameOverComp);
