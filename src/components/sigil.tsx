import { hot } from 'react-hot-loader';
import * as React from 'react';
import "./gfx.scss";
import {Window} from './window';
import { ClippySingleton, Screen } from './clippy';
import {Color, ColorPicker} from './color_picker';
import Progress from './progress';
import Chat from './chat';

type CanvasMouseEvent = React.MouseEvent<HTMLCanvasElement, MouseEvent>;

interface EditorState {
	drawing: boolean;
	brushSize: number;
	done: boolean;
	showChat: boolean;
}

const topRight: [number, number] = [Math.cos(degToRad(30)), Math.sin(degToRad(30))];

function degToRad(n: number) {
	return n * Math.PI / 180;
}

const shape: [number, number][] = [
	topRight,
	[-topRight[0], -topRight[1]],
	[ 0, 1 ],
	[topRight[0], -topRight[1]],
	[-topRight[0], topRight[1]],
	[ 0, -1 ],
	topRight,
];

function scaledPoint([x, y]: [number, number]): [number, number] {
	return [ x*100 + 300, y*100 + 240];
}

class GfxEditor extends React.Component<{}, EditorState> {
	public state = {
		drawing: false,
		brushSize: 10,
		done: false,
		showChat: false,
	}

	private progress: number;
	private startTime: number;
	private canvas: HTMLCanvasElement;
	private gfx_context: CanvasRenderingContext2D;

	private reset() {
		this.setState({ });
	}

	private onMouseDown(_event: CanvasMouseEvent) {
		if(this.state.drawing){
			return;
		}
		ClippySingleton.playSound("sigil");
		this.setState({ drawing: true });
		this.startTime = Date.now();
		window.requestAnimationFrame(() => this.drawMore());
	}

	private async drawMore() {
		this.progress = (Date.now() - this.startTime) / 20000;
		this.updateDrawing(this.progress);
		if(this.progress < 1) {
			window.requestAnimationFrame(() => this.drawMore());
		}
		else {
			await ClippySingleton.speak("Compilation started!");
			this.setState({done: true});
			await ClippySingleton.delay(2000);
			this.setState({showChat: true});
		}
	}

	private onMouseMove(event: CanvasMouseEvent) {}

	private updateDrawing(progress: number) {
		this.gfx_context.clearRect(0, 0, this.canvas.width, this.canvas.height);
		const context = this.gfx_context;
		context.strokeStyle = "#ff0000";
		context.lineJoin    = "round";
		context.lineWidth   = 5;
		context.beginPath();

		let p1 = scaledPoint(shape[0]);
		context.moveTo(p1[0], p1[1]);
		for(let i = 0; i < 6; i++ ) {
			let point = scaledPoint(shape[i+1]);
			if(progress > (i+1) / 6) {
				context.lineTo(point[0], point[1]);
			}
			else {
				let segment = (progress - i/6) * 6;
				let prev_point = scaledPoint(shape[i]);
				let nx = prev_point[0] + (point[0] - prev_point[0]) * segment;
				let ny = prev_point[1] + (point[1] - prev_point[1]) * segment;
				context.lineTo(nx, ny);
				break;
			}
		}

		context.stroke()
	}

	private setCanvas(canvas: HTMLCanvasElement) {
		if(canvas) {
			this.canvas = canvas;
			this.gfx_context = canvas.getContext("2d");
		}
	}

	private changeColor(c: Color) {
	}

	private async submitPicture() {
		this.gfx_context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}

	private progressBar() {
		if(this.state.done) {
			return <Progress/>
		}
		return <></>

	}

	private chatWindow() {
		if(this.state.showChat) {
			return <Chat />
		}
		return <></>
	}

	public render() {
		let brushSize = this.state.brushSize;
		return (
			<Window
				title="🖌️Paint Box Jr."
				onReset={() => this.reset()} >
				<div className='toolbar'>
					Brush size: <input value={brushSize} onChange={(e) => this.setState({brushSize: Number.parseInt(e.target.value) || 0})} 
						pattern="[0-9][0-9]*" />
					Brush color:
					<ColorPicker color={{r: "255", g: "0", b: "0", a: "255" }}
						onColorChange={(c) => this.changeColor(c)} />
					<button onClick={() => this.submitPicture()}><strong>Submit</strong></button>
				</div>
				<canvas id="gfx-canvas"
					width="640"
					height="480"
					ref={(canvas) => this.setCanvas(canvas)}
					onMouseMove={(e) => this.onMouseMove(e)}
					onMouseDown={(e) => this.onMouseDown(e)}
					>
				</canvas>
				{this.progressBar()}
				{this.chatWindow()}
			</Window>
		)
	}
}

declare let module: object;
export const Sigil = hot(module)(GfxEditor);
