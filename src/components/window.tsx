import * as React from 'react';
import "./window.scss";
import { hot } from 'react-hot-loader';

interface WindowProps {
	children: React.ReactNode;
	title: string;
	onReset: () => void;
}

class WindowComp extends React.Component<WindowProps, {}> {

	private resetButton() {
		if(process.env.NODE_ENV !== "production") {
			return (<button onClick={() => this.props.onReset()}>Reset</button>);
		}
		return (<></>);
	}

	public render() {
		return (
			<div id='window-frame'>
				<div className="title">{this.props.title}</div>
				<div className='window-body'>
					{this.props.children}
				</div>
				<div className="status-bar">
					{this.resetButton()}
					Ready...
				</div>
			</div>
		)
	}
}

declare let module: object;
export const Window = hot(module)(WindowComp);
