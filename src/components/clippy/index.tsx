import { hot } from 'react-hot-loader';
import * as React from 'react';
import Bubble from '../bubble';
import {Howl} from 'howler';

import './clippy.scss';

const image = require('./clippy.gif');

export let ClippySingleton: ClippyComp;

export enum Screen {
	Design,
	Gfx,
	Sfx,
	Sigil,
	GameOver,
}

interface ClippyState {
	currentMessage: string|null;
	bubble: any|null;
	text: string;
	visible: boolean;
}

interface ClippyProps {
	screenChanger: (s: Screen) => void;
}

// TODO: https://github.com/danilosetra/react-responsive-spritesheet#examples
class ClippyComp extends React.Component<ClippyProps, ClippyState> {
	public state = {
		currentMessage: null,
		bubble: null,
		text: "",
		visible: false,
	}

	private ambience1: Howl|null = null;
	private ambience2: Howl|null  = null;

	public componentDidMount() {
		ClippySingleton = this;
	}

	public setText(text: string) {
		this.setState({ text });
	}

	public addText(m: string) {
		this.setState({ text: this.state.text + m });
	}

	public async speak(message: string): Promise<void> {
		return new Promise<void>(async resolve => {
			await this.prompt(message, <a href='#' onClick={() => resolve()}>Continue...</a>);
			resolve();
		});
	}

	public async prompt(speak: string, bubble: React.ReactNode): Promise<void> {
		return new Promise<void>(resolve => {
			const utterance = new window.SpeechSynthesisUtterance(speak);
			utterance.addEventListener("end", () => {
				resolve();
			});
			window.speechSynthesis.speak(utterance);
			this.setState({
				currentMessage: speak,
				visible: true,
				bubble: <Bubble><p>{speak}</p>{bubble}</Bubble>
			});
		});
	}

	public playSound(path: string): Promise<void> {
		return new Promise(resolve => {
			const sound = new Howl({
				src: ['src/components/audio/' + path + '.m4a', 'src/components/audio/' + path + '.ogg'],
				loop: false,
				onend: () => resolve(),
			});
			sound.play();
		});
	}

	public startAmbience1() {
		if (!this.ambience1) {
			this.ambience1 = new Howl({
				src: ['src/components/audio/ambience1.m4a', 'src/components/audio/ambience1.ogg'],
				loop: true
			});
			this.ambience1.play();
		}
	}

	public startAmbience2() {
		if(this.ambience1) {
			this.ambience1.stop();
		}
		if (!this.ambience2) {
			this.ambience2 = new Howl({
				src: ['src/components/audio/ambience2.m4a', 'src/components/audio/ambience2.ogg'],
				loop: true
			});
			this.ambience2.play();
		}
	}

	public stopAmbience2() {
		if (this.ambience2) {
			this.ambience2.stop();
		}
	}

	async delay(time: number): Promise<void> {
		return new Promise(resolve => setTimeout(() => resolve(), time));
	}

	async promptInput(message: string, inputs: [string, string][]): Promise<string> {
		return new Promise(resolve => {
			this.prompt(
				message,
				<div>
					{inputs.map(([v, name], i) => <p key={i}><a href='#' onClick={() => resolve(v)}>{name}</a><br /></p> )}
				</div>
			);
		});
	}

	public changeScreen(screen: Screen) {
		this.props.screenChanger(screen);
	}

	public removeBubble() {
		this.setState({bubble: null});
	}

	public render() {
		if(!this.state.visible) {
			return (<></>);
		}
		return (
			<div id='clippy'>
				<div className='avatar'>
					<img src={image} />
				</div>
				<div id='clippy-bubble'>{this.state.bubble}</div>
			</div>
		);
	}
}

declare let module: object;
export const Clippy = hot(module)(ClippyComp);
