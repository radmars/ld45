# Setup

`npm install -D`

Dev:

`npm run start-dev`

http://localhost:8080

Prod:

`npm build`

then host `dist/` e.g. with `python -m SimpleHTTPServer 8000`

Based on
https://github.com/vikpe/react-webpack-typescript-starter
